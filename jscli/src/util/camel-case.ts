export namespace UtilCamelCase {
  export function camelCase(word: string): string {
    return word.split('-').reduce((str, word) => str.charAt(0).toUpperCase() + str.slice(1) + word[0].toUpperCase() + word.slice(1));
  }
}
